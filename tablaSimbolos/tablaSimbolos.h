/* 
 * File:   tablaSimbolos.h
 * Author: pablo
 *
 * Created on 22 de octubre de 2014, 17:55
 */

#ifndef TABLASIMBOLOS_H
#define	TABLASIMBOLOS_H
#include "../nodo/nodo.h"
#ifdef	__cplusplus
extern "C" {
#endif

    void ambitoLocal(gdsl_hash_t global,Nodo *n, FILE *f, FILE* fo);


#ifdef	__cplusplus
}
#endif

#endif	/* TABLASIMBOLOS_H */

