#include "tablaSimbolos.h"
#include <stdio.h>

void ambitoLocal(gdsl_hash_t global, Nodo *n, FILE *f, FILE* fo) {
    /*Inicializamos las variables*/
    int valor = 0;
    char buffer[MAX_BUFFER] = "";
    char *tok = (char *) malloc(MAX_BUFFER * sizeof (char));
    char *tok1 = (char *) malloc(MAX_BUFFER * sizeof (char));
    /*Inicializamos la tabla del ambito local e insertamos el ambito*/
    gdsl_hash_t local = gdsl_hash_alloc("Local", *allocNodo, *borrarNodo, *getKey, NULL, 0);
    gdsl_hash_insert(local, (void *) n);
    /*Seguimos leyendo el fichero*/
    while (!feof(f)) {
        fgets(buffer, 20, f);
        tok = strtok(buffer, "\t\r\n");
        tok1 = strtok(NULL, "\t\r\n");
         n = crearNodo("INIT",1);
        if (!tok1) {
            n = cambiaNodo(n, tok, 0);
            if (!gdsl_hash_search(local, tok) && !gdsl_hash_search(global, tok)) {
                /*Fallo de búsqueda*/
                fprintf(fo, "%s\t-1\n", tok);
                continue;
            }
            if (gdsl_hash_search(local, tok)) {
                /*Acierto de búsqueda en tabla local*/
                n = gdsl_hash_search(local, tok);
                fprintf(fo, "%s\t%i\n", getKey((void*) n), getValue((void*) n));
                continue;
            }
            if (gdsl_hash_search(global, tok)) {
                /*Acierto de búsqueda en tabla global*/
                n = gdsl_hash_search(global, tok);
                fprintf(fo, "%s\t%i\n", getKey((void*) n), getValue((void*) n));
                n=NULL;
                continue;
            }
            continue;
        } else {
            valor = atoi(tok1);
            if (valor == -999) {
                /*cerrar ámbito local*/
                gdsl_hash_free(local);
                fprintf(fo, "cierre\n");
                break;
            }
            if (valor<-1&&valor !=-999) {
                fprintf(fo, "No se puede crear un ambito local dentro de otro.\n");
                continue;
            }
            /*Vamos a insertar en el ámbito local si no está ya insertado*/
            n=cambiaNodo(n,tok,valor);
            if(!gdsl_hash_search(local,tok)){
                /*Insertar en el ámbito global el elemento*/
                if(!gdsl_hash_insert(local,(void*)n)){
                    /*No hay memoria para insertar el elemento en la tabla local, indicar en el fichero*/
                    fprintf(fo,"No hay memoria para insertar mas elementos en la tabla local, una pena.\n");
                    continue;
                }else{
                    /*Escribir la inserción en el fichero de salida*/
                    fprintf(fo,"%s\n",getKey((void*)n));
                    continue;
                }
                continue;
            }else{
                /*El elemento ya está en la tabla local, asique hay que decir en el fichero de salida que no se pudo insertar*/
                fprintf(fo,"-1\t%s\n",tok);
            }
            
        }
    }
    return;
}