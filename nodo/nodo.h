/* 
 * File:   nodo.h
 * Author: jose
 *
 * Created on 22 de octubre de 2014, 12:51
 */

#ifndef NODO_H
#define	NODO_H
#include <stdlib.h>
#include <string.h>
#define MAX_BUFFER 100
#include "../hash/gdsl_hash.h"
#ifdef	__cplusplus
extern "C" {
#endif

 typedef struct {
        char* key;
        int value;
    }Nodo;
    /*Funcion que copia un nodo en otro (alocando memoria para el segundo)
     *Recibe el nodo a copiar y devuelve la nueva zona de memoria con el nodo copiado
     */
    void *allocNodo(void *e);
    /*Funcion que crea un nodo a partir de ina clave y valor indicados por argumento
     *Recibe la clave y el valor y devuelve la nueva zona de memoria con el nodo copiado
     */
    Nodo *crearNodo(char * key, int value);
    /*Funcion que recibe un nodo y devuelve su clave o NULL en caso de error*/
    const char * getKey(void *el);
    /*Funcion que recibe un nodo y devuelve su valor o -35535 en caso de error*/
    int getValue(void *el);
    /*Función que libera la memoria del nodo que recibe como argumento*/
    void borrarNodo(void* e);
    /*Funcion que actualiza los datos de un nodo para el que ya se ha reservado memoria*/
    Nodo *cambiaNodo(Nodo *n,char *key, int value);

#ifdef	__cplusplus
}
#endif

#endif	/* NODO_H */

