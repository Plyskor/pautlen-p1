/* 
 * File:   main.c
 * Author: Jose
 *
 * Created on 14 de octubre de 2014, 18:17
 */

#include <stdio.h>
#include <stdlib.h>
#include "hash/gdsl_hash.h"
#include "nodo/nodo.h"
/*
 * 
 */
int main(int argc, char** argv) {
    /*Inicializacion de variables*/
    char buffer[MAX_BUFFER]="";
    char *tok = (char *)malloc(MAX_BUFFER*sizeof(char));
    char *tok1 = (char *)malloc(MAX_BUFFER*sizeof(char));
    int valor=0;
    Nodo *n ;
    FILE *fo;
    if(argc!=2&&argc!=3){
        printf("Introduzca los argumentos correctamente. Deben de ser:\n<1>\tFichero de texto de entrada\n<2>\tFichero de salida (por defecto es txt/output.txt)\n");
    }
    gdsl_hash_t global =gdsl_hash_alloc ("Global",*allocNodo,*borrarNodo,*getKey,NULL,0);
    FILE *f = fopen(argv[1], "r");
    if (argc==2){
       fo= fopen("txt/output.txt", "w");
    }
    if(argc==3){
        fo= fopen(argv[2], "w");
    }
    if(!f){
        printf("Error al leer el fichero de texto. Asegúrese que se encuentra en el directorio /txt.\n");
    }
    
    /*Bucle de lectura y actuación*/
    while(!feof(f)){
        fgets(buffer,20,f);
        tok=strtok(buffer,"\t\r\n");
        tok1=strtok(NULL,"\t\r\n");
        n = crearNodo("INIT",1);
        if(!tok1){
            /*buscar en la tabla hash, y actualizar el fichero de salida*/
            n=cambiaNodo(n,tok,0);
            if(!gdsl_hash_search(global,tok)){
                /*Fallo de búsqueda*/
                fprintf(fo,"%s\t-1\n",tok);
                continue;
            }else{
                /*Acierto de búsqueda*/
                n=gdsl_hash_search(global,tok);
                fprintf(fo,"%s\t%i\n",getKey((void*)n),getValue((void*)n));
                n=NULL;
                continue;
            }
            continue;
        }
        valor=atoi(tok1);
        if(valor<-1){
            /*crear ámbito local*/
            /*A Insertar*/
            n=cambiaNodo(n,tok,valor);
            if(!gdsl_hash_search(global,tok)){
                /*Insertar en el ámbito global el elemento*/
                if(!gdsl_hash_insert(global,(void*)n)){
                    /*No hay memoria para insertar el elemento en la tabla, indicar en el fichero*/
                    fprintf(fo,"No hay memoria para insertar mas elementos en la tabla, una pena.\n");
                    continue;
                }else{
                    /*Escribir la inserción en el fichero de salida*/
                    fprintf(fo,"%s\n",getKey((void*)n));
                    
                }
                
            }else{
                /*El elemento ya está en la tabla, asique hay que decir en el fichero de salida que no se pudo insertar*/
                fprintf(fo,"-1\t%s\n",tok);
                continue;
            }
            ambitoLocal(global,n,f,fo);
            continue;
        }else{
            /*añadir al ámbito global*/
            n=cambiaNodo(n,tok,valor);
            if(!gdsl_hash_search(global,tok)){
                /*Insertar en el ámbito global el elemento*/
                if(!gdsl_hash_insert(global,(void*)n)){
                    /*No hay memoria para insertar el elemento en la tabla, indicar en el fichero*/
                    fprintf(fo,"No hay memoria para insertar mas elementos en la tabla, una pena.\n");
                    continue;
                }else{
                    /*Escribir la inserción en el fichero de salida*/
                    fprintf(fo,"%s\n",getKey((void*)n));
                    continue;
                }
                continue;
            }else{
                /*El elemento ya está en la tabla, asique hay que decir en el fichero de salida que no se pudo insertar*/
                fprintf(fo,"-1\t%s\n",tok);
            }
            
        }
    }
    gdsl_hash_free(global);
    return (EXIT_SUCCESS);
}



