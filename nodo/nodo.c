/* 
 * File:   nodo.c
 * Author: jose
 *
 * Created on 22 de octubre de 2014, 12:51
 */

#include <stdio.h>
#include "nodo.h"
/*
 * 
 */
void *allocNodo(void *no){
        if(!no) return NULL;
        Nodo *n=(Nodo *)no;
        Nodo *ret = (Nodo *)malloc(sizeof(Nodo));
        if(!ret) return NULL;
        ret->key=(char *)malloc(strlen(n->key)*sizeof(char));
        if(!n->key||!ret->key) return NULL;
        strcpy(ret->key,n->key);
        ret->value=n->value;
        return (void *)ret;
    }
Nodo *crearNodo(char *key,int value){
    if(!key) return NULL;
        Nodo *ret = (Nodo *)malloc(sizeof(Nodo));
        if(!ret) return NULL;
        ret->key=(char *)malloc(strlen(key)*sizeof(char));
        if(!ret->key)return NULL;
        strcpy(ret->key,key);
        ret->value=value;
        return ret;
}
Nodo *cambiaNodo(Nodo *n,char *key, int value){
    if(!key||!n)return NULL;
    n->key=(char*)realloc(n->key,strlen(key)*sizeof(char));
    if(!n->key)return NULL;
    n->value=value;
    strcpy(n->key,key);
    return n;
}
int getValue(void *no){
    if(!no)return -35535;
    Nodo *n=(Nodo *)no;
    return n->value;
}
const char * getKey(void *no){
    if(!no)return NULL;
    Nodo *n = (Nodo *)no;
    return n->key;
}
void borrarNodo(void *n){
/*
    if(!n)return;
*/
    Nodo *no=(Nodo *)n;
    free(no->key);
    free(no);
    return;
}
